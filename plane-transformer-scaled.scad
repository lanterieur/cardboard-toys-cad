$fa=1;
$fs=0.4;
// modules
module prism(l, w, h){
      polyhedron(//pt 0        1        2        3        4        5
              points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
              faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
              );
 }
 module myrotate(a, orig) {
    translate(orig)
    rotate(a)
    translate(-orig)
    children();
}

// functions
function yRotationAroundHipPivot() = $t>0.5?(-2*$t*180):(2*$t*180);
function doubleRotateTranslation()=[(bh-as/2-po)*sin(yRotationAroundHipPivot()),0,po+(bh-as/2-po)*cos(yRotationAroundHipPivot())];
// vars
double_rotate_on = true;

body_depth = 4;
bd = body_depth;

body_center_width = 8;
bcw = body_center_width;

body_height = 8;
bh = body_height;

pivot_width = bd/2;
pw = pivot_width;
pivot_offset = bh/10;
po = pivot_offset;

hips_inner_width = 2;
hiw = hips_inner_width;

hips_top_height = 3;
hth = hips_top_height;
hips_lower_height = 2.5;
hlh = hips_lower_height;
hips_height = hth + hlh;
hh = hips_height;

head_radius = 1.5;
hr = head_radius;
head_cap_pivt_height = 1;
hcph = head_cap_pivt_height;
head_cap_length = bh+hth/2;
hcl = head_cap_length;
head_cap_radius = 17/10*hr;
hcr = head_cap_radius;

arm_side = bd*3/4;
as = arm_side;
arm_length = bh+hth;
al = arm_length;

leg_length = 15;
ll = leg_length;
leg_width = 3;
lw = leg_width;
leg_radius = lw/2;
lr = leg_radius;

feet_height = 2;
fh = feet_height;
feet_length = 5/3*lw;
fl = feet_length;
feet_width = lw;
fw = feet_width;

wing_depth = 0.5;
wd = wing_depth;
wing_span = 3*(pw+as);
ws = wing_span;
wing_height = 2*(bh+hth);
wh = wing_height;

bridge_height = 2;

//body
color("brown") translate([0,0,bh/2]) cube([bd,bcw,bh],center=true);
//head
color("grey") translate([0,0,bh+hr]) sphere(hr);


// Side Arm Wing assembly
myrotate([0,yRotationAroundHipPivot(),0],[0,bcw+pw,po]){
    
    //RIGHT SIDE
    
    //pivot side
color("magenta") translate([0,-(bcw+pw)/2,bh/2]) cube([bd,pw,bh],center=true);
    //wing
color("teal") rotate([0,180,0]) translate([bd/2,-ws-bcw/2,-bh-hr*4-bridge_height/2]) prism(wd,ws,wh);
    //shoulder
    if(!double_rotate_on){
color("blue") translate([0,-(bcw+2*pw+as)/2,bh-(as/2)]) cube(as, center=true);
    //arm
color("cyan") translate([0, -(bcw+2*pw+as)/2,-(al-bh)-as]) cylinder(al,as/2,as/2);
    }
    
    // LEFT SIDE
    
    //pivot hip
color("magenta") translate([0,(bcw+pw)/2,bh/2]) cube([bd,pw,bh],center=true);
    //wing
color("teal") rotate([0,180,180]) translate([-bd/2-wd,-ws-bcw/2,-bh-hr*4-bridge_height/2])  prism(wd,ws,wh);
    //shoulder
    if(!double_rotate_on){
color("blue") translate([0,(bcw+2*pw+as)/2,bh-(as/2)]) cube(as, center=true);
    //arm
color("cyan") translate([0, (bcw+2*pw+as)/2,-(al-bh)-as]) cylinder(al,as/2,as/2);
    }
    
    // BRIDGE

color("teal") translate([-bd/2-wd/2,0,bh+4*hr]) rotate([0,180,180])  cube([wd,bcw,bridge_height],center=true);    
}

if(double_rotate_on){
    // RIGHT SIDE
    
    //arm
    color("cyan") translate([0,-bcw/2-pw-as/2,-al]) translate(doubleRotateTranslation()) cylinder(al,as/2,as/2);
// shoulder
color("blue") translate([0,-bcw/2-pw-as/2,0]) translate(doubleRotateTranslation()) cube(as, center=true);
    
    // LEFT SIDE
    
    //arm
    color("cyan") translate([0,bcw/2+pw+as/2,-al]) translate(doubleRotateTranslation()) cylinder(al,as/2,as/2);
// shoulder
color("blue") translate([0,bcw/2+pw+as/2,0]) translate(doubleRotateTranslation()) cube(as, center=true);
}


//hips
color("green") translate([0,0,-hth/2]) cube([bd,bcw,hth],center=true);
color("green") translate([0,0,-hth-hlh/2]) cube([bd,hiw,hlh],center=true);
//legs
color("orange") translate([0,hiw/2+lr,-hh/2-ll]) cylinder(ll,lr,lr);
color("orange") translate([0,-hiw/2-lr,-hh/2-ll]) cylinder(ll,lr,lr);
//feet
color("lime") translate([lw/3,hiw/2+lr,-ll-hlh-hth/2]) cube([fl,lw,fh],center=true);
color("lime") translate([lw/3,-hiw/2-lr,-ll-hlh-hth/2]) cube([fl,lw,fh],center=true);

color("lime") rotate([0,0,90]) translate([hiw/2+lr-fw/2,-5.5/3*lw,-ll-hlh-hth/2-fh/2]) prism(fw,lw*2/3,fh);
color("lime") translate([5.5/3*lw,-hiw/2-lr-fw/2,-ll-hlh-hth/2-fh/2]) rotate([0,0,90]) prism(fw,lw*2/3,fh);
// head cap
myrotate(
[0,-180+yRotationAroundHipPivot(),0],
[-bd/2,0,bh]
){
    difference(){
translate([0,0,bh+hcph/2]) cylinder(hcl,hcr,wd);
        translate([0,0,-(1/10*hcl)+bh+hcph/2]) cylinder(9/10*hcl,hcr*19/20,wd*9/10);
    }
translate([-bd/2+wd/2,0,bh+hcph/2]) cube([wd,bcw,hcph],center=true);
}